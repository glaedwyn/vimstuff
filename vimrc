set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

"General
Plugin 'VundleVim/Vundle.vim'

"Coding stuff
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
Plugin 'lifepillar/vim-mucomplete'
Plugin 'xavierd/clang_complete'
Plugin 'luochen1990/rainbow'

"Colorschemes
Plugin 'szorfein/darkest-space'
Plugin 'morhetz/gruvbox'

call vundle#end()

filetype plugin on

"MuComplete stuff
set shortmess+=c
set belloff+=ctrlg
let g:mucomplete#enable_auto_at_startup = 1

"clang_complete stuff
set noinfercase
set completeopt-=preview
set completeopt+=menuone,noselect
let g:clang_library_path = '/usr/lib/libclang.so'
let g:clang_user_options = '-std=c++17'
let g:clang_complete_auto = 1
let g:mucomplete#enable_auto_at_startup = 1

"rainbow stuff
let g:rainbow_active = 1

"Syntastic stuff
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_compiler_options='--std=c++17 -Wall'

"My Stuff
syntax on
set number
set autoindent
set mouse=a
map <F2> :NERDTreeToggle<CR>

"Colorscheme stuff
"colorscheme darkest-space
let g:gruvbox_italic=1
colorscheme gruvbox





